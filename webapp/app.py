import os
import json

from flask import Flask 


app = Flask(__name__, static_url_path='')

base_dir = os.path.dirname(os.path.realpath(__file__))

def load_data():
    data = json.load(open(os.path.join(base_dir, 'data/records.json'), 'rt'))
    data = [d for d in data if len(d)==4]
    for d in data:
        d['url'] = f"<a href={d['url']}>{d['url']}</a>"
        issue_num = int(d['issue'].split('_')[-1].split('.')[0])
        d['issue'] = f"<a href=https://www.pythonweekly.com/archive/{issue_num}.html>Issue {issue_num}</a>"
    return {'data': data}


@app.route('/', methods=['GET'])
def index():
    """Records UI."""
    return app.send_static_file('records.html')


@app.route('/data')
def get_data():
    #return app.send_static_file('data.txt')
    return json.dumps(load_data())


if __name__=='__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)